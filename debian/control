Source: dangerzone
Section: contrib/utils
Priority: optional
Maintainer: Peymaneh <peymaneh@posteo.net>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-setuptools,
               python3-pyside2.qtcore,
               python3-pyside2.qtgui,
               python3-pyside2.qtwidgets,
               python3-appdirs,
               python3-click,
               python3-xdg,
               python3-requests,
               python3-termcolor
Standards-Version: 4.6.1.0
Homepage: https://dangerzone.rocks
Vcs-Git: https://salsa.debian.org/pkg-privacy-team/dangerzone.git
Vcs-Browser: https://salsa.debian.org/pkg-privacy-team/dangerzone
Rules-Requires-Root: no

Package: dangerzone
Architecture: any
Multi-Arch: foreign
Depends: podman,
         uidmap,
         pdftk-java,
         curl,
         python3-pyside2.qtcore,
         python3-pyside2.qtgui,
         python3-pyside2.qtwidgets,
         python3-appdirs,
         python3-click,
         python3-xdg,
         python3-requests,
         python3-termcolor,
         ${misc:Depends},
         ${python3:Depends}
Description: convert potentially dangerous documents to safe PDFs
 PRIVACY NOTICE: This package will download a data from https://docker.io and
 https://gitlab.com at installation time in order to set up a sandboxed
 environment for document conversion.
 .
 Dangerzone works like this: You give it a document that you don't know if you
 can trust (for example, an email attachment). Inside of a sandbox, Dangerzone
 converts the document to a PDF (if it isn't already one), and then converts
 the PDF into raw pixel data: a huge list of RGB color values for each page.
 Then, in a separate sandbox, Dangerzone takes this pixel data and converts it
 back into a PDF.
 .
 Some features:
  - Sandboxes don't have network access, so if a malicious document can
    compromise one, it can't phone home
  - Dangerzone can optionally OCR the safe PDFs it creates, so it will have a
    text layer again
  - Dangerzone compresses the safe PDF to reduce file size
  - After converting, Dangerzone lets you open the safe PDF in the PDF viewer
    of your choice, which allows you to open PDFs and office docs in Dangerzone
    by default so you never accidentally open a dangerous document
 .
 Supported input types:
  - PDF (.pdf)
  - Microsoft Word (.docx, .doc)
  - Microsoft Excel (.xlsx, .xls)
  - Microsoft PowerPoint (.pptx, .ppt)
  - ODF Text (.odt)
  - ODF Spreadsheet (.ods)
  - ODF Presentation (.odp)
  - ODF Graphics (.odg)
  - Jpeg (.jpg, .jpeg)
  - GIF (.gif)
  - PNG (.png)
  - TIFF (.tif, .tiff)
