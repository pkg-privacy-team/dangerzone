Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: dangerzone
Upstream-Contact: info@firstlook.media
Source: https://github.com/firstlookmedia/dangerzone
Comment: Postinst-script downloads an container base image for Alpine Linux,
 which itself distributes only OSI-approved packages, from https://docker.io
 and additionally fetches the GPL v2 licensed pdftk-java from https://gitlab.com
 For a detailed list of packages being fetched see `container/Dockerfile`.

Files: *
Copyright: 2020-2022 First Look Media
License: Expat

Files: debian/*
Copyright: 2022 Peymaneh <peymaneh@posteo.net>
License: Expat
Comment: Debian packaging licensed same as upstream

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
